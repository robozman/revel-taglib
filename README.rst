revel-taglib
============
Rust bindings for TagLib's C++ API.

These bindings were build to be used with `Revel`_.

.. _Revel: https://gitlab.com/Robozman/revel
