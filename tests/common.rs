use std::process::Command;

pub fn setup_flac() {

    let files_to_create = ["test-noalbumartist.wav",
                           "test-ALBUMARTIST.wav", "test-ALBUM_ARTIST.wav"];

    let input_wav = ["test-noalbumartist.wav", "test-ALBUMARTIST.wav",
                     "test-ALBUM_ARTIST.wav"];

    
    Command::new("touch")
        .args(&files_to_create)
        .output()
        .expect("failed to execute process");


    let flac_args = ["--endian=little",
                     "--sign=signed",
                     "--channels=1",
                     "--bps=8",
                     "--sample-rate=44000"];
    
    for file in input_wav.iter() {
        Command::new("flac")
            .args(&flac_args)
            .arg(file)
            .output()
            .expect("failed to execute process");
    }

    let metaflac_args = ["--set-tag=TITLE=test_title",
                         "--set-tag=ARTIST=test_artist",
                         "--set-tag=ALBUM=test_album",
                         "--set-tag=GENRE=test_genre",
                         "--set-tag=TRACKNUMBER=6",
                         "--set-tag=YEAR=420",];

    Command::new("metaflac")
        .args(&metaflac_args)
        .arg("test-noalbumartist.flac")
        .output()
        .expect("failed to execute process");
    Command::new("metaflac")
        .args(&metaflac_args)
        .arg("--set-tag=ALBUMARTIST=test_albumartist")
        .arg("test-ALBUMARTIST.flac")
        .output()
        .expect("failed to execute process");
    Command::new("metaflac")
        .args(&metaflac_args)
        .arg("--set-tag=ALBUM ARTIST=test_albumartist")
        .arg("test-ALBUM_ARTIST.flac")
        .output()
        .expect("failed to execute process");
}

pub fn setup_mp3() {
    let files_to_create = ["test-noalbumartist.mp3", "test-albumartist.mp3"];

    Command::new("touch")
        .args(&files_to_create)
        .output()
        .expect("failed to execute process");

    for file in &files_to_create {
        let mid3v2_args = ["--artist=test_artist",
                           "--album=test_album",
                           "--song=test_title",
                           "--comment=DESCRIPTION:test_description",
                           "--genre=test_genre",
                           "--year=420",
                           "--track=6/9"];
        Command::new("mid3v2")
            .args(&mid3v2_args)
            .arg(file)
            .output()
            .expect("failed to execute process");

    }
    

    
    let mid3v2_args = ["--TPE2", "test_albumartist", "test-albumartist.mp3"];
    Command::new("mid3v2")
        .args(&mid3v2_args)
        .output()
        .expect("failed to execute process");
}

pub fn cleanup_flac() {
    let files_to_delete = ["test-noalbumartist.wav",
                           "test-ALBUMARTIST.wav", "test-ALBUM_ARTIST.wav",
                           "test-noalbumartist.flac",
                           "test-ALBUMARTIST.flac", "test-ALBUM_ARTIST.flac"];

    Command::new("rm")
        .args(&files_to_delete)
        .output()
        .expect("failed to execute process");
}

pub fn cleanup_mp3() {
    let files_to_delete = ["test-noalbumartist.mp3", "test-albumartist.mp3"];

    Command::new("rm")
        .args(&files_to_delete)
        .output()
        .expect("failed to execute process");
}
