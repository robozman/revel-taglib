extern crate revel_taglib;

mod common;


fn test_generic(file: &revel_taglib::File) {
    let tag = &file.tag;
    assert_eq!(tag.is_empty, false);
    assert_eq!(tag.title, "test_title");
    assert_eq!(tag.artist, "test_artist");
    assert_eq!(tag.album, "test_album");
    assert_eq!(tag.genre, "test_genre");
    assert_eq!(tag.year, 420);
    assert_eq!(tag.track, 6);
    let audio_properties = &file.audio_properties;
    assert_eq!(audio_properties.length, 0);
    assert_eq!(audio_properties.bitrate, 0);
}


#[test]
fn test_mp3() {
    common::cleanup_mp3();
    common::setup_mp3();

    let mp3_noalbumartist = revel_taglib::File::new(&String::from("test-noalbumartist.mp3")).unwrap();
    test_generic(&mp3_noalbumartist);
    assert_eq!(mp3_noalbumartist.tag.album_artist, None);
    let mp3_albumartist = revel_taglib::File::new(&String::from("test-albumartist.mp3")).unwrap();
    test_generic(&mp3_albumartist);
    assert_eq!(mp3_albumartist.tag.album_artist, Some(std::string::String::from("test_albumartist")));


    common::cleanup_mp3();
}

#[test]
fn test_flac() {
    common::cleanup_flac();
    common::setup_flac();
    let flac_noalbumartist = revel_taglib::File::new(&String::from("test-noalbumartist.flac")).unwrap();
    test_generic(&flac_noalbumartist);
    assert_eq!(flac_noalbumartist.tag.album_artist, None);
    #[allow(non_snake_case)]
    let flac_ALBUMARTIST = revel_taglib::File::new(&String::from("test-ALBUMARTIST.flac")).unwrap();
    test_generic(&flac_ALBUMARTIST);
    assert_eq!(flac_ALBUMARTIST.tag.album_artist, Some(std::string::String::from("test_albumartist")));
    #[allow(non_snake_case)]
    let flac_ALBUM_ARTIST = revel_taglib::File::new(&String::from("test-ALBUM_ARTIST.flac")).unwrap();
    test_generic(&flac_ALBUM_ARTIST);
    assert_eq!(flac_ALBUM_ARTIST.tag.album_artist, Some(std::string::String::from("test_albumartist")));
    common::cleanup_flac();
}
