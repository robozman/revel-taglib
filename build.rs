extern crate cpp_build;

#[cfg(feature = "pkg-config")]
extern crate pkg_config;


fn main() {
    cpp_build::Config::new().flag("-std=c++14").build("src/lib.rs");
    if !build_pkgconfig() {
        println!("cargo:rustc-flags=-ltag");
    }
}

#[cfg(not(feature = "pkg-config"))]
fn build_pkgconfig() -> bool {
    false
}

#[cfg(feature = "pkg-config")]
fn build_pkgconfig() -> bool {
    if pkg_config::find_library("taglib").is_err() {
        panic!("Could not find taglib via pkgconfig");
    }
    true
}
